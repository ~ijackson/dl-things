/*   	Printable Filament Spool Spindle, teeth
        by Ian Jackson License, GPL v2 or later
                based on Printable Filament Spool Spindle for Reprap
		by Travis Howse <tjhowse@gmail.com>
		2011.   License, GPL v2 or later
		Based on:
		Makerbottable Filament Spool v2.0
		by Randy Young <dadick83@hotmail.com>
		2010, 2011.   License, GPL v2 or later
**************************************************/

include <Libs.scad> //  Libs.scad is @ http://www.thingiverse.com/thing:6021
include <FilamentSpool.scad> //  FilamentSpool.scad is @ http://www.thingiverse.com/thing:8317

// WARNING base of this is multiple overlapping surfaces and must be
// sliced off

module SpindleTeeth(d=0.01, basewidth=12, baseheight=15, baseyoff=0)
{
	translate([-10,-4.5, -d])
	difference()
	{
		union()
		{
			translate([0,-baseheight/2+4+baseyoff,0])
				cube([10.03,baseheight,basewidth]);
			intersection() {
				translate([0,-2,0])
					Spindle(10.5,11,8,0,0,0,5,0,[1.5,18,5]);
				translate([5,-3,0])
					cube([50,15,12]);
			}
		}
		translate([9,3.8,-4]) minkowski(){
			cylinder(r=1.5,h=40);
			cube([20,1,0.1]);
		}
	}
}
