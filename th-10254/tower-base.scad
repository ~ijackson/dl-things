
motorwidth=35.2;
motorheight=36.5;
totalheight=65;

pillarthick=8;
sidethick=2.5;
archthick=6.5;
frameextra=3.5;
framesplay=1;
botleftgap=4.5;
botleftstand=0.75;
archoutwards=(pillarthick-archthick)/sqrt(8);

dovebasecutcylz=4;
dovebasecutcylr=10;

d=0.01;

mw2=motorwidth/2;

include <Libs.scad> //  Libs.scad is @ http://www.thingiverse.com/thing:6021

module corner() {
	$fn=30;
	frameheight= motorheight + frameextra;
	slopeheight= totalheight - frameheight;
	slope = (mw2 + archoutwards - framesplay)/slopeheight;
	echo(sqrt(2)*slope);

	translate([-mw2,-mw2,0]) union(){
		difference(){
			union(){
				cylinder(r=pillarthick/2, h=frameheight);
				translate([0,0,frameheight])
					sphere(r=pillarthick/2);
			}
			translate([d,d,-1])
				cube([mw2-1,mw2-1,frameheight+pillarthick+2]);
		}
		intersection(){
			multmatrix
			   ([	[	1,	0,	slope,	-archoutwards ],
				[	0,	1,	slope,	-archoutwards ],
				[	0,	0,	1, frameheight	],
				[	0,	0,	0,	1 	]])
				translate([0,0,-frameextra])
				cylinder(r=archthick/2,
					 h=slopeheight+frameextra);
			union() {
				cylinder(r=pillarthick/2, h=frameheight);
				translate([-100,-100,frameheight])
					cube([200,200,100]);
			}
		}
	}
}

module halfside() {
	spacesz = (motorwidth - pillarthick/2*2) / 4;
	panelheight = spacesz + sidethick;
	panelbasez = motorheight+pillarthick/4-panelheight;
	translate([0,-mw2,0]) {
		translate([-mw2,-sidethick,0])
			cube([motorwidth,sidethick,sidethick]);
		difference(){
			translate([-mw2,-sidethick, panelbasez])
				cube([mw2,sidethick,panelheight]);
			translate([-mw2+pillarthick/3, -sidethick, panelbasez])
				rotate([0,45,0])
				translate([0,-1,0])
				cube([spacesz * sqrt(2),
				      sidethick+2,
				      spacesz * sqrt(2)]);
		}
		intersection(){
			for (xz=[[-mw2+pillarthick/3-sidethick, 0,
					panelbasez+sidethick],
				 [0, 0, panelbasez + sidethick/sqrt(2)]]) {
				translate(xz)
				translate([0,-sidethick,0])
					rotate([0,45,0])
					translate([0,0,-sidethick])
					cube([100, sidethick, sidethick]);
			}
			translate([-mw2,-sidethick,0])
				cube([motorwidth,sidethick,
					motorheight+pillarthick]);
		}
	}
}

module towerbase() {
	difference(){
		union(){
			for (angle=[0,90,180,270])
				rotate([0,0,angle]) corner();
			for (angle=[0,90,180]) {
				rotate([0,0,angle]) halfside();
				rotate([0,0,angle]) mirror([1,0,0]) halfside();
			}
		}
		multmatrix([[	-1,	0,	0, -mw2 - botleftstand ],
			[	0,	1,	0,	-100	],
			[	1,	0,	1, -100 + botleftgap ],
			[	0,	0,	0,	1	] ])
			cube([100,200,100]);
	}
	translate([0,0,totalheight])
		rotate([0,-90,0])
		intersection(){
			dovetail(height=10.1, male=true);
			translate([dovebasecutcylz,0,-100])
				cylinder(r=dovebasecutcylr,h=200);
		};
}

//intersection(){
	towerbase();
//	translate([-100,-100,0]) cube([200,200,32]);
//}
