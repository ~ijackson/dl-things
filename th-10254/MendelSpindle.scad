/*   	Printable Filament Spool Spindle for Reprap
		by Travis Howse <tjhowse@gmail.com>
		2011.   License, GPL v2 or later
		Based on:
		Makerbottable Filament Spool v2.0
		by Randy Young <dadick83@hotmail.com>
		2010, 2011.   License, GPL v2 or later
**************************************************/

include <Libs.scad> //  Libs.scad is @ http://www.thingiverse.com/thing:6021
include <FilamentSpool.scad> //  FilamentSpool.scad is @ http://www.thingiverse.com/thing:8317

module MendelSpindle(mainwidth=6,clipwidth=10,extenddist=40)
{
	difference()
	{
		union()
		{
			translate([0,-3,0]) cube([10,15,12]);
			intersection() {
				translate([0,-2,0])
					Spindle(10.5,11,8,0,0,0,5,0,[1.5,18,5]);
				translate([5,-3,0])
					cube([50,15,12]);
			}
			translate([-extenddist-5,0,0])
				cube([extenddist+10,9,mainwidth]);
			translate([-extenddist,9,0]) rotate([0,0,150])
				ClipBar(mainwidth=mainwidth,
					clipwidth=clipwidth);
		}
		translate([9,3.8,-4]) minkowski(){
			cylinder(r=1.5,h=40);
			cube([20,1,0.1]);
		}
	}
}

module clipbarcylinders(r,h) {
		rotate([0,0,-13])
		{
			translate([7,11.1,-1]) cylinder(r=r,h=h);
			translate([7+37.5,11.1,-1]) cylinder(r=r,h=h);
		}
}

module ClipBar(mainwidth,clipwidth)
{
	difference()
	{
		union() {
			cube([57,10,mainwidth]);
			intersection() {
				clipbarcylinders(r=6,h=clipwidth);
				cube([57,10,clipwidth]);
			}
			minkowski() {
				rotate([0,0,3.5]) translate([-7,1,0])
					cube([27,0.1,mainwidth-1]);
				cylinder(r=4,h=1);
			}
		}
		translate([0,0,-1]) clipbarcylinders(r=3.5,h=clipwidth+2);
	}
}

MendelSpindle();

