/*   	Printable Filament Spool Spindle for Huxley
        by Ian Jackson License, GPL v2 or later
                based on Printable Filament Spool Spindle for Reprap
		by Travis Howse <tjhowse@gmail.com>
		2011.   License, GPL v2 or later
		Based on:
		Makerbottable Filament Spool v2.0
		by Randy Young <dadick83@hotmail.com>
		2010, 2011.   License, GPL v2 or later
**************************************************/

include <Libs.scad> //  Libs.scad is @ http://www.thingiverse.com/thing:6021
include <SpindleTeeth.scad>

d=0.01;

module HuxleySpindleTower(height=125, sideways=27.5,
		doveheight=10, dovewidth=17, mainthick=6, mainwidth=10) {

	curvemidintheethx = -4;
	curvemidr = sideways + curvemidintheethx;
	sticklen = height-doveheight-curvemidr + 1;

	translate([curvemidintheethx, -curvemidr, 0]) {
		intersection(){
			difference(){
				cylinder(h=mainthick, r=curvemidr+mainwidth/2);
				translate([0,0,-1])
					 cylinder(h=mainthick+2,
						  r=curvemidr-mainwidth/2);
			}
			translate([-100,-d,-50])
				cube([100,100,100]);
		}
	}
	translate([-sideways-mainwidth/2, -sticklen-curvemidr, 0])
		cube([mainwidth, sticklen, mainthick]);

	SpindleTeeth(baseheight=20,baseyoff=-1.5);

	translate([-sideways, -height, doveheight/2-d]) {
		rotate([0,0,90])
		difference(){
			translate([d,-dovewidth/2,d-5])
				cube([doveheight+d*2, dovewidth,
				      doveheight-d*2]);
			dovetail(height=doveheight+1, male=false);
		}
	}
}

difference(){
	mirror([1,0,0]) HuxleySpindleTower();
	translate([-200,-200,-1]) cube([400,400,1.005]);
}
