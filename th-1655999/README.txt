                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1655999
Ambiguous Solid by TimLParis is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

<p>Model of an ambiguous solid.
<p>Look at it from one direction it appears to be a cube, and from the opposite it appears to be a cylinder. Hold it up to a mirror and you see both at the same time.
<p>The 4x model is a group of four of the shapes merged together to produce the illusion of either 4 connected cubes or four overlapping cylinders.

# Print Settings

Printer Brand: LulzBot
Printer: TAZ 5
Supports: Yes
Resolution: 0.2
Infill: 20%

Notes: 
This was very difficult for me to print as removing the supports without damaging the print was a problem.

# Post-Printing

Take care when removing the support material as it is easy to split the model.

# How I Designed This

<p>After seeing this video (not my video), I had to try and model this shape.
<p>I created a cube in Blender, subdivided the sides so I would be able to smoothly deform it. I worked from the final viewing angle and distorted the cube until it looked like a cylinder. I switched to the opposite viewing angle and made sure the object looked like a cube.

<iframe src="//www.youtube.com/embed/oWfFco7K9v8" frameborder="0" allowfullscreen></iframe>